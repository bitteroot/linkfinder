from unittest import TestCase
from linkfinder.source import SimpleDirectorySource
from linkfinder.driver import GeventDriver
from linkfinder.runner import get_runner
import os

DIR_PATH = os.path.join(os.path.dirname(__file__), "data/splunk_app_stream")


class TestRunner(TestCase):

    def setUp(self):
        super(TestRunner, self).setUp()
        driver = GeventDriver()
        source = SimpleDirectorySource(path=DIR_PATH)
        self.runner = get_runner(driver, source)

    def tearDown(self):
        super(TestRunner, self).tearDown()
        self.runner = None

    def test_run(self):
        print DIR_PATH
        self.assertIsNotNone(self.runner)
        self.runner.run()
        self.runner.print_results()
